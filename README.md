# Arduino EDID Programmer

This repostiory contains an Arduino sketch (firmware) for programming Electronic Display Identification Data to an EEPROM chip over I2C. It's designed to work with the Scanner Shield (see Boards/ScannerShield) and encode the necessary EDID for the Holographic Video Monitor. The same code could be adapted to any I2C connected EEPROM (using the Wire library) and data block by changing the "phoenixDatString" constant in the source file and I2C address.

Any questions can be directed to byuholography@gmail.com

# License

All files in this repository are Copyright 2019 BYU ElectroHolography Research Group and are licensed under GNU General Public License v3 ([https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html))

![GPL v3 Logo](https://www.gnu.org/graphics/gplv3-127x51.png "GPL v3")
