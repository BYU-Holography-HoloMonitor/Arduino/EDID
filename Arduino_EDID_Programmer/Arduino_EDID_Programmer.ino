///////////////////////////////////////////////////////////////
//                                                           //
//         BYU Holography EDID Parser/Reader/Writer          //
//                                                           //
///////////////////////////////////////////////////////////////
//                                                           //
//  Author: Andrew Henrie                                    //
//  Date: Wed 21 Aug 2019                                    //
//                                                           //
//  Copy and paste one of the action options provided below  //
//  into the initialization value of the global variable     //
//  "action" to indicate what you'd like to do.              //
//                                                           //
//  Assuming that the Arduino, Holographic Video Monitor     //
//  Arduino Shield, the VGA/DVI I2C adapter, and the MixAmp  //
//  CCA are all connected, perform one of the following:     //
//                                                           //
//  READ_ONLY                                                //
//   - read the current EDID on the EEPROM chip              //
//                                                           //
//  PARSE_AND_READ                                           //
//   - test-parse a Phoenix EDID Designer .dat file and      //
//     read the current EDID                                 //
//                                                           //
//  PARSE_WRITE_AND_READ                                     //
//   - parse a Phoenix EDID Designer .dat file, write it to  //
//     the EEPROM chip, and read it back for verification    //
//                                                           //
///////////////////////////////////////////////////////////////
//  
//  Copyright 2016-2019 BYU ElectroHolography Research Group
//  
//  This is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  
//  Send questions to: byuholography@gmail.com
/////////////////////////////////////////////////////////////////////////





#include <Wire.h>

#define  READ_ONLY             0
#define  PARSE_AND_READ        1
#define  PARSE_WRITE_AND_READ  2

int action = PARSE_AND_READ; // PARSE_WRITE_AND_READ; // READ_ONLY; // 

//  If you want to flash your own EDID, you will have to
//  copy-paste the Phoenix EDID Designer .dat file here
//  and put double-quotation marks around each line with
//  a semicolon after the last double-quotation mark; like so:

byte PhoenixDATstring[] = 
  "EDID BYTES:"
  "0x   00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F"
  "    ------------------------------------------------"
  "00 | 00 FF FF FF FF FF FF 00 0B 35 05 00 DE AD BE EF"
  "10 | 22 1D 01 03 68 27 21 78 08 8F 30 A3 55 49 98 27"
  "20 | 14 50 54 00 00 00 01 01 01 01 01 01 01 01 01 01"
  "30 | 01 01 01 01 01 01 40 9C 20 70 F0 DD 23 C0 10 60"
  "40 | 11 00 83 49 11 00 00 1E 00 00 00 FC 00 42 59 55"
  "50 | 5F 48 56 4D 4F 4E 49 54 4F 52 00 00 00 10 00 0A"
  "60 | 0A 0A 0A 0A 0A 0A 0A 0A 0A 0A 0A 0A 00 00 00 10"
  "70 | 00 0A 0A 0A 0A 0A 0A 0A 0A 0A 0A 0A 0A 0A 00 C2"
  ""; // optimized_52hololines_Wed21Aug2019_thesis.dat





// parse support functions

boolean chkHexFmt( byte c )
{
  // check that a character can be hexadecimal

  return ('0' <= c && c <= '9') || ('A' <= c && c <= 'F') || ('a' <= c && c <= 'f');
}

byte char2nybble( byte c )
{
  // convert a hexadecimal character to a number between 0-15

  if( '0' <= c && c <= '9' )
    return c - '0';
  else if( 'a' <= c && c <= 'f' )
    return c - 'a' + 10;
  else if( 'A' <= c && c <= 'F' )
    return c - 'A' + 10;
  else
    return 0x0e; // a unique error code
}

byte hex2byte( byte charU, byte charL )
{
  // convert two hexadecimal characters into a number between 0-255

  if( chkHexFmt( charU ) && chkHexFmt( charL ) )
  {
    return ( char2nybble( charU ) << 4 ) | char2nybble( charL );
  }
  else
  {
    Serial.print( "hex parse error: '" );
    Serial.write( charU );
    Serial.print( "' '" );
    Serial.write( charL );
    Serial.println( "'" );
    return 0xdd; // a unique error code
  }
}





// global config variables

unsigned long I2C_delay = 10; // milliseconds to delay after I2C transfers, for making sure state machines have enough processing time
// if there are intermittent / repeated failures and the hardware is likely good, try multiplying this value by 10

// DVI breakout
int pin_HOT_PLUG = 22; // out
boolean pin_HOT_PLUG_state = LOW;

String inputString = "Press 'h' to toggle hotplug pin (currently low)..."; // a string to hold incoming data
boolean stringComplete = true;





void setup()
{  
  // DVI breakout
  
  pinMode( pin_HOT_PLUG,  OUTPUT );
  digitalWrite( pin_HOT_PLUG, pin_HOT_PLUG_state );
  
  // all uninitialized pins are left as inputs by default
  
  // Serial Communication
  
  Serial.begin( 9600 );
  Wire.begin(); // join i2c bus (address optional for master)
  Serial.flush();
  Serial.println();
  Serial.println( "Alive..." );
  Serial.println();
  
  inputString.reserve(200);


  
  // parse the Phoenix EDID Designer .dat file

  byte EDID[ 128 ];
  if( action == PARSE_AND_READ || action == PARSE_WRITE_AND_READ )
  {
    Serial.println( "Parsing Phoneix EDID .dat file..." );

    int start_i = 126 - 2 * 3; // location of first byte in .dat file
    int line_delta = 54 - 2; // number of characters to advance each line, minus \r \n as these were removed when the file was copied in above

    for (int line_i = 0; line_i < 8; line_i++) // go through each of the 8 data lines
    {
      for (int byte_i = 0; byte_i < 16; byte_i++) // go through each of the 16 bytes
      {
        byte upperNybble = PhoenixDATstring[ byte_i * 3     + line_i * line_delta + start_i ];
        byte lowerNybble = PhoenixDATstring[ byte_i * 3 + 1 + line_i * line_delta + start_i ];
        
        byte c = hex2byte( upperNybble, lowerNybble );
        EDID[ line_i * 16 + byte_i ] = c;
        
        int total_i = byte_i + line_i * 16;
        
        if( total_i < 16 )
          Serial.print( '0' );
        
        Serial.print( total_i, HEX );
        Serial.print( " '" );
        Serial.write( upperNybble );
        Serial.print( "' '" );
        Serial.write( lowerNybble );
        Serial.print( "' " );
        
        if( c < 16 )
          Serial.print( '0' );
        
        Serial.print( c, HEX );
        Serial.println();
      }
    }
    Serial.println( "Parsing done." );
    Serial.println();

    // read back parsed data array
    
    Serial.println( "EDID parsed from file:" );
    for( int i = 0; i < 8; i++ )
    {
      for( int j = 0; j < 16; j++ )
      {
        byte c = EDID[ i * 16 + j ];
        
        if( c < 16 )
          Serial.print( '0' );
        
        Serial.print( c, HEX );
        
        if( j != 15 )
          Serial.print( ' ' );
      }
      Serial.println();
    }
    Serial.println();


    
    // write the EDID data to the EEPROM
    
    if( action == PARSE_WRITE_AND_READ )
    {
      Serial.println( "Writing parsed data to EEPROM (I2C addr 0x50)..." );

      // write 8 bytes at a time; this is the limit for the Microchip 24AA02/24LC02B chip we're using
      for( int i = 0; i < 16; i++ )
      {
        Wire.beginTransmission( 0x50 ); // I2C address 0x50
        Wire.write( byte( i * 8 ) ); // memory address in EEPROM
        for( int j = 0; j < 8; j++ )
        {
          Wire.write( EDID[ i * 8 + j ] );
        }
        Wire.endTransmission();
        delay( I2C_delay );
      }

      Serial.println();

    } // end of write case
  } // end of parse case



  // read back and check the EEPROM data

  Serial.println("Reading data...");

  Wire.beginTransmission( 0x50 );
  Wire.write( byte( 0x00 ) ); // set address pointer to 0x00
  Wire.endTransmission();
  delay( I2C_delay );

  int mismatch_addr = 0x100;

  for( int i = 0; i < 8; i++ )
  {
    Wire.requestFrom( 0x50, 16 );

    for( int j = 0; j < 16; j++ )
    {
      uint8_t data = Wire.read();
      if( EDID[ j + i * 16 ] != data && mismatch_addr == 0x100 )
        mismatch_addr = j + i * 16;
      if( data < 0x10 )
        Serial.print( '0' );
      Serial.print( data, HEX );
      Serial.print( ' ' );
    }
    delay( I2C_delay );

    Serial.print( '\n' );
  }
  Serial.println();


  
  // report on the EDID writing attempt

  if( action == PARSE_WRITE_AND_READ )
  {
    if( mismatch_addr != 0x100 )
    {
      Serial.print( "EDID flash unsuccessful; write/reread discrepancy beginning at address 0x" );
      if( mismatch_addr < 16 )
        Serial.print( "0" );
      Serial.println( mismatch_addr, HEX );
    }
    else
    {
      Serial.println( "EDID flash successful!" );
    }

    Serial.println();
  }
  
  

  Serial.println( "Done." );
}



// main loop doesn't do anything

void loop()
{
  // print the string when a newline arrives:
  // if( stringComplete )
  // {
  //   Serial.println( inputString );
  //   // clear the string:
  //   inputString = "";
  //   stringComplete = false;
  // }
}



// serial listener for HOTPLUG toggling

void serialEvent()
{
  while( Serial.available() )
  {
    // get the new byte:
    char inChar = (char) Serial.read();

    if( inChar == 'h' )
    {
      pin_HOT_PLUG_state = !pin_HOT_PLUG_state;
      digitalWrite( pin_HOT_PLUG, pin_HOT_PLUG_state );
      Serial.println( pin_HOT_PLUG_state );
    }
  }
}
